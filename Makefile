compile:
	cd src && javac gui/Gui.java

jrun: compile
	cd src && java gui.Gui

run: huffman.jar
	chmod +x huffman.jar && ./huffman.jar

huffman.jar: pack

pack: compile
	cd src && jar cvfe ../huffman.jar gui.Gui */*.class */*/*.class */*.png

.PHONY: clean mrproper

clean: 
	@(cd src && rm -rf */*.class */*/*.class && cd ..)

mrproper: clean
	@(rm -rf huffman.jar)