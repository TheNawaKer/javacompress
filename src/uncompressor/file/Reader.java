package uncompressor.file;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import javax.swing.JProgressBar;

import core.Header;
import core.HeaderException;

/**
 * Classe effctuant la lecture d'un fichier compressé et creer le fichier décrompressé
 */
public class Reader {
	/**
	 * l'en-tête du fichier
	 */
	private Header head;
	/**
	 * le fichier compressé (géré bit à bit)
	 */
	private BitReader file;
	/**
	 * le fichier a écrire (fichier décompressé)
	 */
	private OutputStream out;

	/**
	 * Initialise la lecture et l'écriture
	 * @param path le chemin du fichier compressé
	 * @throws IOException si fichier indisponible
	 */
	public Reader(String path) throws IOException {
		file = new BitReader(path);
		head = new Header();
		out = new BufferedOutputStream(new FileOutputStream(path.substring(0,
				path.length() - 4)));
	}

	/**
	 * Lit le fichier compressé et écrit le fichier décompressé
	 * @param progressBar la barre de progression
	 * @throws IOException si fichier indisponible
	 * @throws HeaderException si header incorrect
	 */
	public void read(JProgressBar progressBar) throws IOException,	HeaderException {
		readHeader(progressBar);
		HashMap<String, Character> mapKey = new HashMap<String, Character>();
		for (char key : head.getHashcode().keySet()) {
			mapKey.put(head.getHashcode().get(key).intern(), key);
		}

		StringBuilder  hash = new StringBuilder ();
		int bit;
		Character carac;
		int cpt = 0;
		for(int i=0;i<head.getFlush();i++)
			file.readBit();
		while (true) {
			bit = file.readBit();
			if (bit == -1)
				break;
			hash.append((char)bit);
			if ((carac = mapKey.get(hash.toString().intern())) != null) {
				out.write(carac);
				hash.setLength(0);
				out.flush();
				progressBar.setValue(cpt++);
			}
		}
		out.flush();
		out.close();
		file.close();


	}

	/**
	 * Lit le Header
	 * @param progressBar la barre de progression
	 * @throws IOException si fichier indisponible
	 * @throws HeaderException si header incorrect
	 */
	public void readHeader(JProgressBar progressBar) throws IOException, HeaderException {
		String huf = "";
		for (int i = 0; i < 3; i++) {
			huf += file.readChar();
		}
		if (!huf.equals("huf")) {
			throw new HeaderException();
		}
		progressBar.setMaximum(file.readInt()/2);
		head.setFlush(file.read());
		int nb = file.readInt();
		for (int i = 0; i < nb; i++) {
			char carac = file.readChar();
			int nbo = file.readInt();
			String code = file.readCode(nbo);
			head.addHash(carac, code);
		}

	}
}
