package uncompressor.file;
/*
 * Classe permettant de lire un fichier bit a bit
 *
 * Auteur: yannick . parmentier @ univ-orleans . fr
 *
 */
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Classe gerant le fichier compressé à décompressé et le lit bit a bit
 */
class BitReader {
	/**
	 * le flux d'entree (donnees d'un fichier)
	 */
    InputStream f;
    /**
     * le buffer
     */
    int bits;
    /**
     * le masque (debute a 10000000) et sera decale de 1 bit a chaque lecture
     */
    int mask;

    /**
     * Initialise la Lecture du fichier compressé
     * @param file le fichier compressé
     * @throws IOException si fichier indisponible
     */
    BitReader(String file) throws IOException{
	f = new BufferedInputStream(new FileInputStream(file));
	bits = 0;
	mask = 0;
    }

    int readBit() throws IOException{
	// methode qui lit le prochain bit dans le flux d'entree

	if (mask == 0) {
	    // on est arrive au bout du masque
	    // on place dans le buffer les 8 prochains bits (un octet)
	    bits = f.read();
	    // reinitialisation du masque a la valeur hexadecimale 80 = 8.16 = 128 = 10000000
	    mask = 0x80;
	    // a la fin du flux on retourne -1
	    if (bits==-1) return(-1);
	}

	if ((bits & mask) == 0) {
	    // le bit selectionne par le masque est 0 (0 & 1 == 0)

	    // decalage du masque pour la prochaine lecture
	    mask = mask >> 1;
	    return 0;
	}
	else {
	    // le bit selectionne par le masque est 1 (1 & 1 == 1)

	    // decalage
	    mask = mask >> 1;
	    return 1;
	}
    }

    /**
     * Lit le code d'un caractère coder sur n bit
     * @param size le nombre de bit a lire
     * @return le code
     * @throws IOException si fichier indisponible
     */
    public String readCode(int size) throws IOException{
		String code="";
		int s=size;
		for(int i=0;i<Math.ceil(size/8.0);i++){
			for(int j=7;j>=0;j--){
				if(s==0){
					break;
				}else{
					int elem =readBit();
					if(elem!=-1){
						code+=(char)elem;
					}else{
						break;
					}
				}
				s--;
			}
		}
		mask=0;
		return code;
	}

    /**
     * Lit un entier
     * @return un entier
     * @throws IOException si fichier indisponible
     */
    public int readInt() throws IOException{
		char a =(char) f.read();
		char b =(char) f.read();
		char c =(char) f.read();
		char d =(char) f.read();
		int f = 0;
		f &= ~(0xff << 24);
		f |= ( a << 24 );
		f |= ( b << 16 );
		f |= ( c <<  8 );
		f |= ( d      );

		return f;
	}

    /**
     * Lit un caractère
     * @return un caractère
     * @throws IOException si fichier indisponible
     */
    public char readChar() throws IOException{
		char a = (char)f.read();
		char b = (char)f.read();
		char f = 0;
		f &= ~(0xff << 8);
		f |= ( a <<  8 );
		f |= ( b      );
		return f;
	}

    /**
     * ferme le fichier compressé
     * @throws IOException si fichier indisponible
     */
    void close() throws IOException {
	f.close();
    }

	public int read() throws IOException {
		return f.read();
	}
}
