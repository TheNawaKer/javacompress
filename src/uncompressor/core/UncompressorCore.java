package uncompressor.core;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JProgressBar;

import uncompressor.file.Reader;
import core.HeaderException;

/**
 * Classe gerant la décompression d'un fichier
 */
public class UncompressorCore {

	/**
	 * Lance la décompression du fichier
	 * @param path le chemin vers le fichier compressé
	 * @param progressBar la barre de progression
	 * @throws IOException si fichier indisponible
	 * @throws HeaderException si header incorrect
	 * @throws FileNotFoundException si le fichier est introuvable
	 */
	public void uncompressor(String path,JProgressBar progressBar) throws IOException, HeaderException, FileNotFoundException{
 			Reader reader=new Reader(path);
			reader.read(progressBar);
	}

	public static void main(String[] args){
		UncompressorCore uncompress= new UncompressorCore();
		try {
			uncompress.uncompressor("D:/projet/JavaCompress/ressources/GUTINDEX.ALL.txt.huf", null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeaderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
