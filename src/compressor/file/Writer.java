package compressor.file;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JProgressBar;

import compressor.core.Tree;
import core.Header;

/**
 * Classe qui gère l'ecriture du fichier compressé
 */
public class Writer {
	/**
	 * l'en-tête du fichier
	 */
	private Header head;
	/**
	 * Le fichier (gère bit à bit)
	 */
	private WriterBit file;
	/**
	 * le chemin du fichier
	 */
	private String path;

	/**
	 * Initialise l'écriture du fichier compressé
	 * @param path le chemin du fichier
	 * @param tree l'arbre d'huffman
	 * @throws IOException si fichier indisponible
	 */
	public Writer(String path,Tree tree) throws IOException
	{
		this.path=path;
		this.head=new Header(tree);
		file=new WriterBit(this.path+".huf");
	}

	/**
	 * Ecrit le fichier compressé
	 * @param progressBar la barre de progression
	 * @throws IOException si fichier indisponible
	 */
	public void writeFile(JProgressBar progressBar) throws IOException{
		writeHeader(progressBar.getMaximum());
		InputStream in=new BufferedInputStream(new FileInputStream(path));
		int carac=0;
		int cpt=progressBar.getValue();
		for(int i=0;i<head.getFlush();i++){
			file.writeBit(0);
		}
		while((carac=in.read())!=-1){
			writeHashcode((char)carac);
			progressBar.setValue(cpt++);
		}
		in.close();
		file.close();
	}


	/**
	 * Ecrit le header
	 * @param size la taille du fichier a compressé
	 * @throws IOException si fichier indisponible
	 */
	public void writeHeader(int size) throws IOException{
		file.writeCharBit('h');
		file.writeCharBit('u');
		file.writeCharBit('f');
		file.writeIntBit(size);
		file.write(head.getFlush());
		file.writeIntBit(head.getHashcode().size());
		for(char carac: head.getHashcode().keySet()){
			file.writeCharBit(carac);
			file.writeIntBit(head.getHashcode().get(carac).length());
			writeHashcode(carac);
			file.flush();
		}
	}

	/**
	 * Ecrit le code d'un caractère
	 * @param carac le coractère
	 * @throws IOException si fichier indisponible
	 */
	public void writeHashcode(char carac) throws IOException{
		String code = head.getHashcode().get(carac);
		for(int i=0;i<code.length();i++){
			file.writeBit(Character.getNumericValue(code.charAt(i)));
		}

	}
}
