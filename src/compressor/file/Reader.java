package compressor.file;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.swing.JProgressBar;

/**
 * Classe qui lit un fichier
 */
public class Reader {
	/**
	 * La liste des caractères et leurs occurences
	 */
	private HashMap<Character, Integer> caracList;
	/**
	 * le fichier
	 */
	private InputStream file;

	/**
	 * initialise le fichier a compresser
	 * @param path chemin vers un fichier
	 * @throws IOException si le fichier est indisponible
	 */
	public Reader(String path) throws IOException {
		file = new BufferedInputStream(new FileInputStream(path));
		caracList = new HashMap<Character, Integer>();
	}

	/**
	 * recupere le caractère suivant et met à jour la table
	 * @throws IOException si le fichier est indisponible
	 */
	public boolean update() throws IOException{
		int caractere = file.read();
		if(caractere==-1)
			return false;
		int nb = 0;
		if (caracList.containsKey((char)caractere)) {
			nb = caracList.get((char)caractere);
		}
		caracList.put((char)caractere, nb + 1);
		return true;
	}

	/**
	 * Rempli la HashMap avec les caractère lu dans le fichier
	 * @param progressBar la barre de progression
	 * @throws IOException si le fichier est indisponible
	 */
	public void generate(JProgressBar progressBar) throws IOException {
		int cpt = 0;
		while (update()) {
			progressBar.setValue(cpt++);
		}
		file.close();
	}

	/**
	 * Renvoi la HashMap caracList
	 * @return caracList
	 */
	public HashMap<Character, Integer> getCaracList() {
		return caracList;
	}

}
