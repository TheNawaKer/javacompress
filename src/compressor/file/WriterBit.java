package compressor.file;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Classe qui gère le fichier à écrire, et écrit bit a bit
 */
public class WriterBit {
	/**
	 * le fichier compressé
	 */
	private OutputStream f;
	/**
	 * le buffer
	 */
	private int octet;
	/**
	 * le nombre de bit dans le buffer
	 */
	private int nb_bit;

	/**
	 * initialise le fichier a écrire
	 * @param file chemin vers un fichier
	 * @throws IOException
	 */
	public WriterBit(String file) throws IOException{
		f=new BufferedOutputStream(new FileOutputStream(file));
		nb_bit=0;
		octet=0;
	}

	/**
	 * ecrit les octets sur le fichier
	 * @param a un bit
	 * @throws IOException si fichier indisponible
	 */
	public void writeBit(int a) throws IOException{
		if(nb_bit==8){
			f.write(octet);
			nb_bit=0;
			octet=0;
		}
		if(a==0){
			octet=octet<<1;
		}else{
			octet=octet <<1|1;
		}
		nb_bit++;
	}


	/**
	 * ecrit un int
	 * @param a l'entier a écrire
	 * @throws IOException si fichier indisponible
	 */
	public void writeIntBit(int a) throws IOException{
		byte[] result = new byte[4];

		result[0] = (byte) (a >> 24);
		result[1] = (byte) (a >> 16);
		result[2] = (byte) (a >> 8);
		result[3] = (byte) (a /*>> 0*/);

		f.write(result);
	}

	/**
	 * ecrit un char
	 * @param a le caractère a écrire
	 * @throws IOException si fichier indisponible
	 */
	public void writeCharBit(char a) throws IOException{
		f.write(a>>8);
		f.write(a);
	}

	/**
	 * effectue le flush (allignement des octets)
	 * @throws IOException si fichier indisponible
	 */
	public void flush() throws IOException{
		if(nb_bit != 8 && nb_bit != 0){
			octet=octet<<(8-nb_bit);
			f.write(octet);
		}else if(nb_bit==8){
				f.write(octet);
		}
		nb_bit=0;
		octet=0;
		f.flush();
	}

	/**
	 * ferme le fichier compressé
	 * @throws IOException si fichier indisponible
	 */
	public void close() throws IOException{
		flush();
		f.close();
	}

	public void write(int octet) throws IOException {
		f.write(octet);

	}

}
