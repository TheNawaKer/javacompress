package compressor.core;

/**
 * Classe qui représente un Noeud dans l'arbre Binaire d'Huffman
 */
public class Node implements Comparable<Node> {

	/**
	 * Noeud gauche
	 */
	private Node left;
	/**
	 * Noeud droite
	 */
	private Node right;
	/**
	 * occurence du caractère
	 */
	private int occurence;
	/**
	 * le caractère
	 */
	private char caractere;

	/**
	 * Creer un Node
	 * @param carac le caractère
	 */
	public Node(char carac){
		caractere=carac;
	}

	/**
	 * Creer un Node
	 * @param occur le nombre d'occurence
	 * @param carac le caractère
	 */
	public Node(int occur,char carac){
		occurence=occur;
		caractere=carac;
	}

	/**
	 * Creer un Node
	 * @param occur le nombre d'occurence
	 * @param carac le caractère
	 * @param r Node Droite
	 * @param l Node Gauche
	 */
	public Node(int occur,char carac,Node r,Node l){
		occurence=occur;
		caractere=carac;
		left=l;
		right=r;
	}


	/**
	 * Recupere le Node gauche
	 * @return Le Node gauche
	 */
	public Node getLeft() {
		return left;
	}

	/**
	 * Remplace le Node gauche par un Node
	 * @param left un Node
	 */
	public void setLeft(Node left) {
		this.left = left;
	}

	/**
	 * Recupere le Node droite
	 * @return Le Node droite
	 */
	public Node getRight() {
		return right;
	}

	/**
	 * Remplace le Node droite par un Node
	 * @param right un Node
	 */
	public void setRight(Node right) {
		this.right = right;
	}

	/**
	 * Recupere l'occurence du Node
	 * @return la valeur de l'occurence
	 */
	public int getOccurence() {
		return occurence;
	}

	/**
	 * change la valeur d'occurence du Node
	 * @param occurence un nombre d'occurence
	 */
	public void setOccurence(int occurence) {
		this.occurence = occurence;
	}

	/**
	 * Recupere le caractère du Node
	 * @return un caractère
	 */
	public char getCaractere() {
		return caractere;
	}

	/**
	 * change le caractère du Node
	 * @param caractere un caractère
	 */
	public void setCaractere(char caractere) {
		this.caractere = caractere;
	}

	/**
	 * Fonction de comparaison de Node
	 * @param o un Node
	 */
	public int compareTo(Node o) {
		int res=0;
		if(o.occurence<occurence)
			res=-1;
		else if(o.occurence>occurence)
			res=1;
		return res;
	}

	/**
	 * Fonction d'affichage d'un Node
	 */
	public String toString(){
		return caractere+": "+Integer.toString(occurence);
    }

	/**
	 * Verifie si le Node est une feuille
	 * @return true si oui, false sinon
	 */
	public boolean isLeaf(){
		return (left==null && right==null);
	}

}
