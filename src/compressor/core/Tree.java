package compressor.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Classe qui représente L'arbre Binaire d'Huffman
 */
public class Tree {
	public final int tri = 4;
	private Node root;

	/**
	 * Renvoi la racine de l'arbre
	 * @return un Node
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * change le Node de la racine
	 * @param root un Node
	 */
	public void setRoot(Node root) {
		this.root = root;
	}

	/**
	 * Genere L'arbre d'Huffman
	 * @param caracList une HashMap de caractere et d'occurence
	 * @throws NoSuchElementException si la HashMap est vide
	 */
	public Tree(HashMap<Character,Integer> caracList) throws NoSuchElementException{
		LinkedList<Node> list=new LinkedList<Node>();
		for (Character mapKey : caracList.keySet()) {
			list.add(new Node(caracList.get(mapKey),mapKey));
		}
		Collections.sort(list);

		while(list.size()>1){
			Node a=list.removeLast();
			Node b=list.removeLast();
			Node tmp=new Node(a.getOccurence()+b.getOccurence(),'^',b,a);
			list.addLast(tmp);
			Collections.sort(list);
		}
		root=list.getFirst();
	}

}
