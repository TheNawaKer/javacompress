package compressor.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.JProgressBar;

import compressor.file.Reader;
import compressor.file.Writer;

/**
 * Classe gerant la compression d'un fichier
 */
public class CompressorCore{

	/**
	 * execute les différentes étapes pour compresser un fichier
	 * @param path emplacement du fichier a compresser
	 * @param progressBar la barre de progression
	 * @throws IOException si le fichier est occupé ou impossible de l'utiliser (lecture/ecriture)
	 * @throws FileNotFoundException si le fichier n'existe pas
	 */
	public void compressor(String path,JProgressBar progressBar) throws IOException, FileNotFoundException{
		Reader reader=new Reader(path);
		reader.generate(progressBar);
		try{
			Tree tree = new Tree(reader.getCaracList());
			Writer write=new Writer(path,tree);
			write.writeFile(progressBar);
		}catch (NoSuchElementException e){

		}

		//TODO exception fichie vide


	}

	/**
	 * Main de test de la classe
	 * @param args
	 */
	public static void main(String[] args){
		CompressorCore c = new CompressorCore();
		JProgressBar j=new JProgressBar();
		try {
			c.compressor("ressources/Noveau document texte (2).txt", j);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
