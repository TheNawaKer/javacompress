package core;

import java.util.HashMap;
import compressor.core.Node;
import compressor.core.Tree;

/**
 * En-tête d'un fichier compressé
 */
public class Header {
	/**
	 * Liste de caractère et leur code
	 */
	private HashMap<Character,String> hashcode;

	/**
	 * le flush du fichier
	 */
	private int flush;

	/**
	 * Genere le Header a partir de l'arbre d'Huffman
	 * @param tree l'arbre d'Huffman
	 */
	public Header(Tree tree){
		hashcode=new HashMap<Character,String>();
		setFlush(0);
		if(tree.getRoot().isLeaf()){
			hashcode.put(tree.getRoot().getCaractere(),"0");
		}else{
			generateHash(tree.getRoot(),"");
		}
		setFlush(8-(flush%8));
		if(flush==8)
			setFlush(7);


	}

	/**
	 * Intialise le Header
	 */
	public Header(){
		hashcode=new HashMap<Character,String>();
		flush=0;
	}

	/**
	 * Genere le code pour un caractère d'un Node (récursif)
	 * @param node un Noeud
	 * @param hash
	 */
	public void generateHash(Node node,String hash){
		if(node.isLeaf()){
			hashcode.put(node.getCaractere(),hash);
			flush+=node.getOccurence()*hash.length();
		}
		else{
			if(node.getLeft()!=null) generateHash(node.getLeft(),hash+"0");
			if(node.getRight()!=null) generateHash(node.getRight(),hash+"1");
		}
	}


	/**
	 * Affiche le Header
	 */
	public void display(){
		for(char carac: hashcode.keySet()){
			System.out.println(carac+": "+hashcode.get(carac));
		}
	}

	/**
	 * Renvoi la valeur du flush
	 * @return le flush
	 */
	public int getFlush() {
		return flush;
	}

	/**
	 * change la valeur du flush
	 * @param flush la valeur
	 */
	public void setFlush(int flush) {
		this.flush = flush;
	}

	/**
	 * Renvoi la liste de caractère et de code
	 * @return hashcode
	 */
	public HashMap<Character, String> getHashcode() {
		return hashcode;
	}

	/**
	 * Ajoute une entrée a la HashMap
	 * @param carac un caractère
	 * @param code un code
	 */
	public void addHash(char carac,String code){
		hashcode.put(carac, code);
	}

}