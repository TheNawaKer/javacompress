package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import uncompressor.core.UncompressorCore;
import compressor.core.CompressorCore;
import core.HeaderException;

@SuppressWarnings("serial")

/**
 * Interface graphique de l'application
 */
public class Gui extends JFrame{

	/**
	 * un fichier
	 */
	private File fichier;
	/**
	 * bouton de selection de fichier
	 */
	private JButton openButton;
	/**
	 * bouton de compression
	 */
	private JButton compressButton;
	/**
	 * bouton de décompression
	 */
	private JButton decompressButton;
	/**
	 * barre de progression
	 */
	private JProgressBar progressBar;

	/**
	 * Creer l'interface et la fenêtre
	 * @param titre le nom de la fenêtre
	 */
	public Gui(String titre){
		super(titre);
		setIconImage(new ImageIcon(this.getClass().getResource("icon.png")).getImage());
		setSize(350,300);
		GridLayout grid = new GridLayout(4,1);
		grid.setHgap(5); //Cinq pixels d'espace entre les colonnes (H comme Horizontal)
		grid.setVgap(5);
		setLayout(grid);
		openButton = new JButton("Ouvrir un fichier");
		compressButton = new JButton("Veuillez selectionner un fichier");
		decompressButton = new JButton("Veuillez selectionner un fichier");
		compressButton.setEnabled(false);
		decompressButton.setEnabled(false);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent event)
			{
				System.exit(0);
			}
		});

		progressBar = new JProgressBar(0,1);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);


		openButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				JFileChooser dialogue = new JFileChooser(new File("."));
				if (dialogue.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
					    fichier = dialogue.getSelectedFile();
					    compressButton.setEnabled(true);
					    decompressButton.setEnabled(true);
					    compressButton.setText("Compresser");
					    decompressButton.setText("Deompresser");
					}

			}
		});


		compressButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				if(fichier!=null){
				    Thread t = new Thread() {
				        public void run() {
				        	progressBar.setMaximum((int)fichier.length()*2);
				        	openButton.setEnabled(false);
				        	compressButton.setEnabled(false);
				        	decompressButton.setEnabled(false);
				        	compressButton.setText("Compression en cours ...");
							CompressorCore compressor = new CompressorCore();
							try {
								compressor.compressor(fichier.getPath(), progressBar);
								JOptionPane.showMessageDialog(null, "Fichier compressé avec succès :D");
							} catch (FileNotFoundException e) {
								JOptionPane.showMessageDialog(null, "Erreur: fichier introuvable");
							} catch (IOException e) {
								JOptionPane.showMessageDialog(null, "Erreur: fichier indisponible ou inexistant (IOEXCEPTION)");
							} finally{
								openButton.setEnabled(true);
								compressButton.setText("Veuillez selectionner un fichier");
								decompressButton.setText("Veuillez selectionner un fichier");
								fichier=null;
								progressBar.setValue(0);

							}

				        }
				      };
				      t.start();


				}
			}
		});

		decompressButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				if(fichier!=null){
				    Thread t = new Thread() {
				        public void run() {
				        	//progressBar.setMaximum((int)fichier.length());
				        	openButton.setEnabled(false);
				        	compressButton.setEnabled(false);
				        	decompressButton.setEnabled(false);
				        	decompressButton.setText("Decompression en cours ...");
							UncompressorCore decompressor = new UncompressorCore();
							try {
								decompressor.uncompressor(fichier.getPath(),progressBar);
								JOptionPane.showMessageDialog(null, "Fichier decompressé avec succès :D");
							} catch (FileNotFoundException e) {
								JOptionPane.showMessageDialog(null, "Erreur: Fichier introuvable");
							} catch (IOException e) {
								JOptionPane.showMessageDialog(null, "Erreur: fichier indisponible ou inexistant (IOEXCEPTION)");
							} catch (HeaderException e) {
								JOptionPane.showMessageDialog(null, "Fichier Corrompu - Header incorrect");
							}finally{
								openButton.setEnabled(true);
								compressButton.setText("Veuillez selectionner un fichier");
								decompressButton.setText("Veuillez selectionner un fichier");
								fichier=null;
								progressBar.setValue(0);

							}

				        }
				      };
				      t.start();


				}
			}
		});

		//getContentPane().add(Limage);
		getContentPane().add(openButton);
		getContentPane().add(compressButton);
		getContentPane().add(decompressButton);
		getContentPane().add(progressBar);
	    setVisible(true);
	}

	/**
	 * Main de l'application
	 * @param args
	 */
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Gui ui = new Gui("Compresseur/Decompresseur Hoffman");
	}
}