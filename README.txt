Groupe: 
	Jimmy Furet
	Yoan Garnier
	Pierrick Pivin
	Sylvain Courtin


Liste des différentes règles du makefile:

compile: compile toutes les classes

jrun: lance la classe principale

run: execute l'executable du programme (jar)

pack: genere l'executable (jar) du programme

clean: supprime tout les .class

mrproper: supprime tout les .class ainsi que l'executable


Documentation:
	lancer le fichier index.html pour avoir acces a la documentation
	elle a été generer grâce au differents commentaires dans le code source (javadoc)

Manuel d'utilisateur:
	un petit manuel expliquant pas à pas comment compresser/decompresser et presente l'interface de l'application
